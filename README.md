# choose-your-own-adventure-book-for-programmers

## Plan for writing the adventure story:
think end states ... things that happen good, bad, neutral.
"choose events to attend or not attend"  
"think game of life" "think different type of developers"

https://twitter.com/dan_abramov/status/618009240742006784
https://twitter.com/dan_abramov/status/618009401736216576

Doug Muise ~
Eating, bathing, having a girlfriend, having an active social life is incidental, 
<br>it gets in the way of code time. Writing code is the primary force that 
<br>drives our lives so anything that interrupts that is is wasteful.

Michael Boyd ~
Lol I gotta say that perhaps he didn’t have his priorities straight

There are many ways to be a programmer.
<br>You could try to live in some scifi future.
<br>Entrepreneural in developer spirit.
<br>Use programming as a form of artistry and expression.
<br>Or stay safe in the present trusted sources.
<br>Make no mistake, any path can be tretcherous.
There are job titles that fit these themes, so as you continue there may be promotion.
<br>Or shifts to different occupation.

Have no fear, once acquired you can put a title on your resume and belay titles
<br>not intended for your trajectory.

The programming world can be the wild wild west. 
<br>Library names from coyote to cowboy. 

A ton can go wrong and a ton can go right.
<br/>You can end up writing a book and becoming famous.
<br/>Or write a book and get a few bad reviews and never a good review.

Oh nooo you were found out that you were making bad architecture decisions.

You were guessing that this new tool would solve all of your problems.
<br>you were right!
<br>you were wrong 😟 .

